<?php require_once "./code.php" ?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>s2 activty</title>
	</head>
	<body>
		<h2>Divisibles of Five</h2>
		<p><?= printDivisiblesOfFive() ?></p>

		<h2>Array Manipulation</h2>
		<p><?php $students = [] ?></p>
		<p><?php $students[] = "John Smith" ?></p>
		<p><?php var_dump($students) ?></p>
		<p><?php echo count($students) ?></p>
		<p><?php $students[] = "Jane Smith" ?></p>
		<p><?php var_dump($students) ?></p>
		<p><?php echo count($students) ?></p>
		<p><?php array_shift($students) ?></p>
		<p><?php var_dump($students) ?></p>
		<p><?php echo count($students) ?></p>
	</body>
</html>